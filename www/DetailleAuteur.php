<!DOCTYPE html>
  <?php
    $sousTitre = $_GET["auteur"];
    $file = ".";
    include $file.'/htmlLoade/entete.php';
  ?>

  <body>
    <?php
      //include($file.'/function/BDfunction.php');
      $Auteur = current(InfoAuteur($_GET["auteur"]));

      echo "<h3 class='Titre'>" . $Auteur['Realisateur'] . "</h3>";
      echo "<section class='DetailleFilm'>";
      echo "<img class='afficheFilm' src='$Auteur[UrlImageRea]' alt='photo du realisateur". $Auteur['Realisateur']."'>";
      echo "<section class='precisionFilm'>";
      echo "<h4> Filmographie</h4>";
      echo "<ul>";
      foreach($Auteur['Filmographie'] as $film){
        echo "<li><a href='./DetailleFilm.php?film=" . $film['IdFilm'] . "'>". $film['Titre'] ."</a></li>";
      }
      echo "</ul>";
      echo "</section>";
    ?>
    </section>
  </body>
<footer>
</footer>

</html>
