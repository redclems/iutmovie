<?php
require("./www/BD/connect.php");
date_default_timezone_set('Europe/Paris');
try{
  // le fichier de BD s'appellera contacts.sqlite3
  $file_db=new PDO('sqlite:contacts.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

  $file_db->exec("create table if not exists REALISATEUR
(
    IdRea number(4),
    NomRea VarChar2(20),
    PrenomRea VarChar2(20),
    UrlImageRea VarChar2(200),
    primary key (IdRea)
);");

  $file_db->exec("create table if not exists UTILISATEUR
(
    Id number(4),
    Pseudo VarChar2(20),
    MotDePasse VarChar2(20),
    RoleU VarChar2(20),
    UrlImageUtil VarChar2(200),
    primary key (Id)
);");

  $file_db->exec("create table if not exists FILM
(
    IdFilm number(4),
    Titre VarChar2(20),
    Genre VarChar2(20),
    IdRea number(4),
    Annee date,
    ResumeF VarChar2(600),
    UrlImageFilm VarChar2(200),
    primary key (IdFilm)
);");


// Insertion des réalisateurs

$realisateur=[
    array('IdRea' => 1,
    'NomRea' => 'Fincher',
    'PrenomRea' => 'David',
    'UrlImageRea' =>  "./images/rea/Fincher.jpg"),
    array('IdRea' => 2,
    'NomRea' => 'Tarantino',
    'PrenomRea' => 'Quentin',
    'UrlImageRea' =>  "./images/rea/Tarantino.jpg"),
    array('IdRea' => 3,
    'NomRea' => 'Nolan',
    'PrenomRea' => 'Christopher',
    'UrlImageRea' =>  "./images/rea/Nolan.jpg"),
    array('IdRea' => 4,
    'NomRea' => 'Kubrick',
    'PrenomRea' => 'Stanley',
    'UrlImageRea' =>  "./images/rea/Kubrick.jpg"),
    array('IdRea' => 5,
    'NomRea' => 'Scott',
    'PrenomRea' => 'Ridley',
    'UrlImageRea' =>  "./images/rea/Scott.jpg"),
    array('IdRea' => 6,
    'NomRea' => 'Zemeckis',
    'PrenomRea' => 'Robert',
    'UrlImageRea' =>  "./images/rea/Zemeckis.jpg"),
    array('IdRea' => 7,
    'NomRea' => 'Leone',
    'PrenomRea' => 'Sergio',
    'UrlImageRea' =>  "./images/rea/Leone.jpg"),
    array('IdRea' => 8,
    'NomRea' => 'Jackson',
    'PrenomRea' => 'Peter',
    'UrlImageRea' =>  "./images/rea/Jackson.jpg"),
    array('IdRea' => 9,
    'NomRea' => 'Ford Coppola',
    'PrenomRea' => 'Francis',
    'UrlImageRea' =>  "./images/rea/Ford_Coppola.jpg"),
    array('IdRea' => 10,
    'NomRea' => 'Gondry',
    'PrenomRea' => 'Michel',
    'UrlImageRea' =>  "./images/rea/Gondry.jpg")
    ];

      $insert="INSERT INTO REALISATEUR (IdRea, NomRea, PrenomRea, UrlImageRea) VALUES (:IdRea, :NomRea, :PrenomRea , :UrlImageRea);";
      $stmt=$file_db->prepare($insert);
      // on lie les parametres aux variables
      $stmt->bindParam(':IdRea',$IdRea);
      $stmt->bindParam(':NomRea',$NomRea);
      $stmt->bindParam(':PrenomRea',$PrenomRea);
      $stmt->bindParam(':UrlImageRea',$UrlImageRea);

      foreach ($realisateur as $r){
        $IdRea=$r['IdRea'];
        $NomRea=$r['NomRea'];
        $PrenomRea=$r['PrenomRea'];
        $UrlImageRea=$r['UrlImageRea'];
        $stmt->execute();
      }


// Insertion des utilisateurs

  $utilisateur=array(
		  array('Id' => 1,
			'Pseudo' => 'Damien',
			'MotDePasse' =>  "mdpd",
      'RoleU' =>  "Admin",
      'UrlImageUtil' =>  "./images/util/admin.png"),
		  array('Id' => 2,
			'Pseudo' => 'Clement',
			'MotDePasse' =>  'mdpc',
      'RoleU' =>  'Utilisateur',
      'UrlImageUtil' =>  "./images/util/util.png")
		  );

  $insert="INSERT INTO UTILISATEUR (Id, Pseudo, MotDePasse, RoleU, UrlImageUtil) VALUES (:Id, :Pseudo , :MotDePasse, :RoleU, :UrlImageUtil);";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':Id',$Id);
  $stmt->bindParam(':Pseudo',$Pseudo);
  $stmt->bindParam(':MotDePasse',$MotDePasse);
  $stmt->bindParam(':RoleU',$RoleU);
  $stmt->bindParam(':UrlImageUtil',$UrlImageUtil);

  foreach ($utilisateur as $u){
    $Id=$u['Id'];
    $Pseudo=$u['Pseudo'];
    $MotDePasse=$u['MotDePasse'];
    $RoleU=$u['RoleU'];
    $UrlImageUtil=$u['UrlImageUtil'];
    $stmt->execute();
  }

// Insertion des films

  $film=array(
		  array('IdFilm' => 1,
      'Titre' => 'Fights Club',
			'Genre' => 'Drame',
			'IdRea' =>  1,
      'Annee' =>  1999,
      'ResumeF' =>  "Insomniaque, désillusionné par sa vie personnelle et professionnelle, un jeune cadre expert en assurances, mène une vie monotone et sans saveur. Face à la vacuité de son existence, son médecin lui conseille de suivre une thérapie afin de relativiser son mal-être. Lors d'un voyage d'affaires, il fait la connaissance de Tyler Durden, une sorte de gourou anarchiste et philosophe. Ensemble, ils fondent le Fight Club. Cercle très fermé, où ils organisent des combats clandestins et violents, destinés à évacuer l'ordre établi.",
      'UrlImageFilm' =>  "./images/fil/Fight_club.jpg"),
		  array('IdFilm' => 2,
      'Titre' => 'Pulp Fiction',
			'Genre' => 'Comedie',
			'IdRea' =>  2,
      'Annee' =>  1994,
      'ResumeF' =>  "L'odyssée sanglante, burlesque et cocasse de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s'entremêlent. ",
      'UrlImageFilm' =>  "./images/fil/Pulp_fiction.jpg"),
		  array('IdFilm' => 3,
      'Titre' => 'Interstellar',
			'Genre' => 'Science-fiction',
			'IdRea' =>  3,
      'Annee' =>  2014,
      'ResumeF' =>  "Alors que la vie sur Terre touche à sa fin, un groupe d’explorateurs s’attelle à la mission la plus importante de l’histoire de l’humanité : franchir les limites de notre galaxie pour savoir si l’homme peut vivre sur une autre planète… ",
      'UrlImageFilm' =>  "./images/fil/Interstellar.jpg"),
		  array('IdFilm' => 4,
      'Titre' => "2001:L'Odysée de l'espace",
			'Genre' => 'Science-fiction',
			'IdRea' =>  4,
      'Annee' =>  1968,
      'ResumeF' =>  "A l'aube de l'Humanité, dans le désert africain, une tribu de primates subit les assauts répétés d'une bande rivale, qui lui dispute un point d'eau. La découverte d'un monolithe noir inspire au chef des singes assiégés un geste inédit et décisif.En 2001, quatre millions d'années plus tard, un vaisseau spatial évolue en orbite lunaire au rythme langoureux du 'Beau Danube Bleu'. A son bord, le Dr. Heywood Floyd enquête secrètement sur la découverte d'un monolithe noir qui émet d'étranges signaux vers Jupiter. ",
      'UrlImageFilm' =>  "./images/fil/Odysee.jpg"),
		  array('IdFilm' => 5,
      'Titre' => 'Blade Runner',
			'Genre' => 'Science-fiction',
			'IdRea' =>  5,
      'Annee' =>  1982,
      'ResumeF' =>  "2019, Los Angeles. La Terre est surpeuplée et l’humanité est partie coloniser l’espace. Les nouveaux colons sont assistés de Replicants, androïdes que rien ne peut distinguer de l'être humain. Conçus comme de nouveaux esclaves, certains parmi les plus évolués s’affranchissent de leurs chaînes et s’enfuient. Rick Deckard, un ancien Blade Runner, catégorie spéciale de policiers chargés de 'retirer' ces replicants, accepte une nouvelle mission consistant à retrouver quatre de ces individus qui viennent de regagner la Terre après avoir volé une navette spatiale. Ces replicants sont des Nexus 6, derniers nés de la technologie développée par la Tyrell Corporation.",
      'UrlImageFilm' =>  "./images/fil/Blade_runner.jpg"),
		  array('IdFilm' => 6,
      'Titre' => 'Le Parrain',
			'Genre' => 'Policier',
			'IdRea' =>  9,
      'Annee' =>  1972,
      'ResumeF' =>  "New York, 1945. Don Vito Corleone règne sur l'une des familles les plus puissantes de la mafia italo-américaine. Virgil Sollozzo, qui dirige la famille Tattaglia, lui propose de s'associer sur un nouveau marché prometteur : le trafic de drogue, mais celui-ci refuse. Don Vito est alors pris pour cible lors d'une tentative d'assassinat en pleine rue. Criblé de balles, il survit par miracle. Son fils Michael, qui s'était toujours tenu à l'écart des affaires familiales, se porte volontaire pour le venger. ",
      'UrlImageFilm' =>  "./images/fil/Parrain.jpg"),
		  array('IdFilm' => 7,
      'Titre' => 'Forest Gump',
			'Genre' => 'Drame',
			'IdRea' =>  6,
      'Annee' =>  1994,
      'ResumeF' =>  "A Savannah, dans l'Etat de Géorgie, Forrest Gump, assis sur un banc public, livre à qui veut l'entendre, l'étrange récit de sa vie mouvementée. Il naît dans un bourg de l'Alabama, affecté d'un quotient intellectuel inférieur à la moyenne et d'une paralysie partielle des jambes. Souvent raillé à l'école, le jeune Forrest se lie d'amitié avec la belle Jenny. Ensemble, ils vont grandir dans l'Amérique des années 1960. ",
      'UrlImageFilm' =>  "./images/fil/Forest_gump.jpg"),
		  array('IdFilm' => 8,
      'Titre' => 'Le Seigneur des Anneaux-Le Retourn du roi',
			'Genre' => 'Fantasy',
			'IdRea' =>  8,
      'Annee' =>  2003,
      'ResumeF' =>  "Guidés par Gollum, Frodon et Sam continuent leur périple vers la montagne du destin, tandis que Gandalf et ses compagnons se retrouvent à Isengard. ",
      'UrlImageFilm' =>  "./images/fil/Seigneur_Roi.jpg"),
		  array('IdFilm' => 9,
      'Titre' => 'The Dark Knight-Le chevalier noir',
			'Genre' => 'Action',
			'IdRea' =>  3,
      'Annee' =>  2008,
      'ResumeF' =>  "Avec l'appui du lieutenant de police Jim Gordon et du procureur de Gotham, Harvey Dent, Batman vise à éradiquer le crime organisé qui pullule dans la ville. Leur association est très efficace, mais elle sera bientôt bouleversée par le chaos déclenché par un criminel psychopathe que les citoyens de Gotham connaissent sous le nom de Joker.",
      'UrlImageFilm' =>  "./images/fil/Batman.jpg"),
		  array('IdFilm' => 10,
      'Titre' => 'Le Bon,la Brute et le Truan',
			'Genre' => 'Western',
			'IdRea' =>  7,
      'Annee' =>  1968,
      'ResumeF' =>  "Un chasseur de primes rejoint deux hommes dans une alliance précaire. Leur but ? Trouver un coffre rempli de pièces d'or dans un cimetière isolé. ",
      'UrlImageFilm' =>  "./images/fil/Bon_Brute_Truan.jpg"),
		  array('IdFilm' => 11,
      'Titre' => 'Inception',
			'Genre' => 'Action',
			'IdRea' =>  3,
      'Annee' =>  2010,
      'ResumeF' =>  "Dom Cobb est un voleur expérimenté, le meilleur dans l'art dangereux de l'extraction : spécialité qui consiste à voler les secrets les plus intimes enfouis au plus profond du subconscient durant une phase de rêve. Très recherché pour ses talents dans l’univers trouble de l’espionnage industriel, Cobb est aussi devenu un fugitif traqué dans le monde entier. Une ultime mission pourrait lui permettre de retrouver sa vie passée, accomplir une « inception ».",
      'UrlImageFilm' =>  "./images/fil/Inception.jpg"),
		  array('IdFilm' => 12,
      'Titre' => "Le Seigneur des Anneaux-La Communauté de l'anneau",
			'Genre' => 'Fantasy',
			'IdRea' =>  8,
      'Annee' =>  2001,
      'ResumeF' =>  "Frodon reçoit l'Anneau de son oncle Bilbo. Sa vie et son monde sont pourtant en danger, car cet anneau appartient à Sauron, le maître des ténèbres. ",
      'UrlImageFilm' =>  "./images/fil/Seigneur_Commu.jpg"),
		  array('IdFilm' => 13,
      'Titre' => 'Apocalypse Now',
			'Genre' => 'Guerre',
			'IdRea' =>  9,
      'Annee' =>  1979,
      'ResumeF' =>  "Durant la guerre du Viêt-nam, le capitaine Willard est contraint de mener une mission périlleuse au Cambodge. Accompagné de quatre soldats, il doit mettre fin au commandement du colonel Kurtz, qui utilise des méthodes jugées trop barbares. ",
      'UrlImageFilm' =>  "./images/fil/Apocalypse_now.jpg"),
		  array('IdFilm' => 14,
      'Titre' => 'Orange mécanique',
			'Genre' => 'Drame',
			'IdRea' =>  4,
      'Annee' =>  1972,
      'ResumeF' =>  "Dans une Angleterre futuriste et inhumaine, un groupe d'adolescents se déchaînent chaque nuit, frappant et violant d'innocentes victimes. Alex, le leader du gang est arrêté et condamné à 14 ans de prison. Il accepte de se soumettre à une thérapie de choc destinée à faire reculer la criminalité. ",
      'UrlImageFilm' =>  "./images/fil/Orange_mecanique.jpg"),
		  array('IdFilm' => 15,
      'Titre' => "Eternal Sunshine of the Spotless Mind",
			'Genre' => 'Drame',
			'IdRea' =>  10,
      'Annee' =>  2004,
      'ResumeF' =>  "L'idylle entre Clementine et Joel a pris fin, en raison de leurs caractères trop différents et de la routine. Pour apaiser ses souffrances, Clementine a recours à Lacuna, un procédé révolutionnaire qui efface certains souvenirs. Désespéré, Joel décide de suivre le même processus. Une nuit, deux techniciens s'y emploient. Mais quand le passé défile dans sa tête, Joel mesure à quel point il aime toujours Clementine.",
      'UrlImageFilm' =>  "./images/fil/Eternal.jpg")
		  );

  $insert="INSERT INTO FILM (IdFilm, Titre, Genre, IdRea, Annee, ResumeF, UrlImageFilm) VALUES (:IdFilm, :Titre, :Genre , :IdRea, :Annee, :ResumeF, :UrlImageFilm);";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':IdFilm',$IdFilm);
  $stmt->bindParam(':Titre',$Titre);
  $stmt->bindParam(':Genre',$Genre);
  $stmt->bindParam(':IdRea',$IdRea);
  $stmt->bindParam(':Annee',$Annee);
  $stmt->bindParam(':ResumeF',$ResumeF);
  $stmt->bindParam(':UrlImageFilm',$UrlImageFilm);

  foreach ($film as $f){
    $IdFilm=$f['IdFilm'];
    $Titre=$f['Titre'];
    $Genre=$f['Genre'];
    $IdRea=$f['IdRea'];
    $Annee=$f['Annee'];
    $ResumeF=$f['ResumeF'];
    $UrlImageFilm=$f['UrlImageFilm'];
    $stmt->execute();
  }

  echo "Insertion en base reussie !";
  // on ferme la connexion
  $file_db=null;
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
