<?php

require("./BD/connect.php");
session_start();
if(isset($_SESSION["id"])){
  if( $_SESSION["role"] == "Admin"){
$titre=$_POST['Titre'];
$genre=$_POST['Genre'];
$nomrea2=$_POST['NomRea'];
$prenomrea2=$_POST['PrenomRea'];
$annee=$_POST['Annee'];
$resume=$_POST['ResumeF'];

$file_db=connect_bd();
$idfilm=$file_db->query("SELECT MAX(IdFilm)idFilm from FILM;");

foreach($idfilm as $val){

    $idreaMAx=$file_db->query("SELECT MAX(IdRea)IdRea from REALISATEUR;");
    foreach($idreaMAx as $val2){
      //echo "iciAuteur";
      $idrea2 = $val2['IdRea']+1;
      $realisateur=array(
        array('IdRea' => $idrea2,
        'NomRea' => $nomrea2,
        'PrenomRea' => $prenomrea2,
        'UrlImageRea' =>  "./images/rea/"
      ));

      $insert="INSERT INTO REALISATEUR (IdRea, NomRea, PrenomRea, UrlImageRea) VALUES (:IdRea, :NomRea, :PrenomRea , :UrlImageRea);";
      $stmt=$file_db->prepare($insert);
       // on lie les parametres aux variables
       $stmt->bindParam(':IdRea',$IdRea);
       $stmt->bindParam(':NomRea',$NomRea);
       $stmt->bindParam(':PrenomRea',$PrenomRea);
       $stmt->bindParam(':UrlImageRea',$UrlImageRea);

      foreach ($realisateur as $r){
        $IdRea=$r['IdRea'];
        $NomRea=$r['NomRea'];
        $PrenomRea=$r['PrenomRea'];
        $UrlImageRea=$r['UrlImageRea'];
        $stmt->execute();
      }
  }


  $id = $val["idFilm"] + 1;
  $Filmes=
    array(
      array('IdFilm' => $id,
      'Titre' => $titre,
      'Genre' => $genre,
      'IdRea' =>  $idrea2 ,
      'Annee' =>  $annee,
      'ResumeF' =>$resume,
      'UrlImageFilm' => "./images/fil/"
    ));

    $insert="INSERT INTO FILM (IdFilm, Titre, Genre, IdRea, Annee, ResumeF, UrlImageFilm) VALUES (:IdFilm, :Titre, :Genre , :IdRea, :Annee, :ResumeF, :UrlImageFilm);";
    $stmt=$file_db->prepare($insert);
    // on lie les parametres aux variables
    $stmt->bindParam(':IdFilm',$IdFilm);
    $stmt->bindParam(':Titre',$Titre);
    $stmt->bindParam(':Genre',$Genre);
    $stmt->bindParam(':IdRea',$IdRea);
    $stmt->bindParam(':Annee',$Annee);
    $stmt->bindParam(':ResumeF',$ResumeF);
    $stmt->bindParam(':UrlImageFilm',$UrlImageFilm);

    foreach ($Filmes as $f){
      //echo "iciFilm";
      $IdFilm=$f['IdFilm'];
      $Titre=$f['Titre'];
      $Genre=$f['Genre'];
      $IdRea=$f['IdRea'];
      $Annee=$f['Annee'];
      $ResumeF=$f['ResumeF'];
      $UrlImageFilm=$f['UrlImageFilm'];
      $stmt->execute();
    }
}
echo "<a href='./IUTMovie.php'> revenire a iut</a>";
}else{
  header('Location: ./IUTMovie.php');
}
}
?>
