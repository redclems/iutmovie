<?php
function film_page($q){
    $html = "<section class='FilmPage' ><h3>" . $q['text'] . "</h3>";
    $html .= "<p><input type='text' name='$q[name]' class='$q[sujet]'></p></section>";

    echo $html;
}

function film_Icon($q){
    $html = "<a href='./DetailleFilm.php?film=" . $q['Id'] . "'>";
    $html .= "<img class='afficheFilm' src='$q[UrlImageFilm]' alt='affiche du film". $q['Titre'] ."'>";
    $html .= "<h3>".$q['Titre']."</h3>";
    $html .= "<section class='infoFilm'>";
    $html .= "<p class='rea'>" . $q['Realisateur'] . "</p>";
    $html .= "<p class='type'>" . $q['TypeFilm'] . "</p>";
    $html .= "</section></a>";

    echo $html;
}
?>
