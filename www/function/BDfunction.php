<?php

require($file."/BD/connect.php");

date_default_timezone_set('Europe/Paris');

function lesFilms($trierPar){
  try{
    $file_db=connect_bd();

		$listeFilm = [];

		$result = $file_db->query('SELECT * from FILM natural join REALISATEUR');

		foreach ($result as $m){
			  $film = array('Id' => $m['IdFilm'], 'Titre' => $m['Titre'], 'UrlImageFilm' => $m['UrlImageFilm'], 'Realisateur' => $m['NomRea'] . "-" . $m['PrenomRea'], 'TypeFilm' => $m['Genre']);
				array_push($listeFilm, $film);
		}

	  $file_db=null;

		return $listeFilm;
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
}

function InfoFilm($arg){

    try{
        $file_db=connect_bd();

        $listeFilm = [];

        $Film=$file_db->query("SELECT DISTINCT Titre, IdRea, UrlImageFilm, NomRea, PrenomRea, Genre, Annee, ResumeF from FILM natural join REALISATEUR WHERE IdFilm=".$arg.";");

        foreach($Film as $m){
            $res = array('Titre' => $m['Titre'], 'UrlImageFilm' => $m['UrlImageFilm'], 'IdRea' => $m['IdRea'] ,'Realisateur' => $m['NomRea'] . "-" . $m['PrenomRea'], 'TypeFilm' => $m['Genre'], 'Annee' => $m['Annee'], 'ResumeFilm' => $m['ResumeF']);
            array_push($listeFilm, $res);
        }
        $file_db=null;
        return $listeFilm;
    }
    catch(PDOException $ex){
        echo $ex->getMessage();
    }
}
function ImageUt($id){
    try{
        $file_db=connect_bd();

        $imageList = [];
        $uti=$file_db->query("SELECT UrlImageUtil, Pseudo from UTILISATEUR WHERE Id=".$id.";");


        foreach($uti as $u){
            $res = array('UrlImageUtil' => $u['UrlImageUtil'], "Pseudo" => $u['Pseudo']);
            array_push($imageList, $res);
        }
        $file_db=null;
        return current($imageList);
    }
    catch(PDOException $ex){
        echo $ex->getMessage();
    }

}

function InfoAuteur($arg){

    try{
        $file_db=connect_bd();

        $listeAuteur = [];

        $auteur=$file_db->query("SELECT * from REALISATEUR WHERE IdRea=".$arg.";");


        foreach($auteur as $m){
            $listes = [];
            $listeFilm=$file_db->query("SELECT IdFilm, Titre from FILM WHERE IdRea=".$arg.";");
            foreach($listeFilm as $u){
              $rus = array('IdFilm' => $u['IdFilm'], 'Titre' => $u['Titre']);
              array_push($listes, $rus);
            }
            $res = array('UrlImageRea' => $m['UrlImageRea'], 'Realisateur' => $m['NomRea'] . "-" . $m['PrenomRea'], 'Filmographie' => $listes);

            array_push($listeAuteur, $res);
        }
        $file_db=null;
        return $listeAuteur;
    }
    catch(PDOException $ex){
        echo $ex->getMessage();
    }
}

function TypeFilm(){
    date_default_timezone_set('Europe/Paris');

    try{
        $file_db=connect_bd();

        $listeTypeFilm = [];

        $types = $file_db->query('SELECT DISTINCT Genre from FILM');

        if (is_array($types) || is_object($types))
        {
          foreach($types as $type){
            $res = array('Genre' => $type['Genre']);
            array_push($listeTypeFilm,$res);
          }
        }
        $file_db=null;
        return $listeTypeFilm;
    }
    catch(PDOException $ex){
        echo $ex->getMessage();
    }
}

function Rechercher($trierPar, $arg){
    date_default_timezone_set('Europe/Paris');
    try{
      $file_db=connect_bd();

      $listeTypeFilm = [];
      $Film=$file_db->query("SELECT DISTINCT IdFilm, Titre, UrlImageFilm, NomRea, PrenomRea, Genre, Annee, ResumeF from FILM natural join  REALISATEUR WHERE Genre='$arg' OR Titre='$arg';");

      if (empty($Film)){

     }else{
       foreach($Film as $m){
           $res = array('Id' => $m['IdFilm'],'Titre' => $m['Titre'], 'UrlImageFilm' => $m['UrlImageFilm'], 'Realisateur' => $m['NomRea'] . "-" . $m['PrenomRea'], 'TypeFilm' => $m['Genre'], 'Annee' => $m['Annee'], 'ResumeFilm' => $m['ResumeF']);
           array_push($listeTypeFilm,$res);
       }
       $file_db=null;
       return $listeTypeFilm;
     }
    }
    catch(PDOException $ex){
        echo $ex->getMessage();
    }
}
