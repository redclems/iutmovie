<?php
require("./BD/connect.php");

$user=$_POST['Pseudo'];
$mdp=$_POST['mdp'];

$file_db=connect_bd();
$idu=$file_db->query("SELECT MAX(Id) id from UTILISATEUR;");
foreach($idu as $val){
  $id= $val["id"] + 1;
  $utilisateur=array(
    array('Id' => $id,
    'Pseudo' => $user,
    'MotDePasse' =>  $mdp,
    'RoleU' =>  "Utilisateur",
    'UrlImageUtil' =>  "./images/util/rand.png"),
    );

  $insert="INSERT INTO UTILISATEUR (Id, Pseudo, MotDePasse, RoleU, UrlImageUtil) VALUES (:Id, :Pseudo , :MotDePasse, :RoleU, :UrlImageUtil);";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':Id',$Id);
  $stmt->bindParam(':Pseudo',$Pseudo);
  $stmt->bindParam(':MotDePasse',$MotDePasse);
  $stmt->bindParam(':RoleU',$RoleU);
  $stmt->bindParam(':UrlImageUtil',$UrlImageUtil);

  foreach ($utilisateur as $u){
    $Id=$u['Id'];
    $Pseudo=$u['Pseudo'];
    $MotDePasse=$u['MotDePasse'];
    $RoleU=$u['RoleU'];
    $UrlImageUtil=$u['UrlImageUtil'];
    $stmt->execute();
  }
}

header('Location: ./IUTMovie.php');
?>
